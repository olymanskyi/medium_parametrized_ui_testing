package ua.tapir.uitests.parametrization.selenium;

import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import ua.tapir.uitests.parametrization.siteconfig.TestedPageContextArgumentsProvider;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Target({ElementType.ANNOTATION_TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@ParameterizedTest
@ArgumentsSource(TestedPageContextArgumentsProvider.class)
@ExtendWith(WebdriverInitCallback.class)


public @interface ParametrizedUiTest {
    /**
     * @return browser to test. Default - all configured browsers.
     * @see TestBrowser
     */
    TestBrowser[] browsers() default {};

    /**
     * Designed to test different paths without test code duplication.
     * For example we test site https://sokyra.party/ with EN and UA locales
     * So it will be "/en" and "/ua" => https://sokyra.party/ua and https://sokyra.party/en
     * Or we just want to test paths "/ua/events" and "/ua/app" for js errors without any other assertions.
     *
     * @return paths to test. Default - all configured locales.
     * @see ua.tapir.uitests.parametrization.siteconfig.SiteLocale
     */
    String[] paths() default {};

    String failOnJsErrors() default "FALSE";

}

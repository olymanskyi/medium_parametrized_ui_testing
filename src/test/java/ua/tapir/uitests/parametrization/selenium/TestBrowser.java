package ua.tapir.uitests.parametrization.selenium;

import com.codeborne.selenide.Browsers;

public enum TestBrowser {

    CHROME(Browsers.CHROME), FIREFOX(Browsers.FIREFOX);
    private String code;

    TestBrowser(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}

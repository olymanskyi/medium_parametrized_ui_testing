package ua.tapir.uitests.parametrization.selenium;

import com.codeborne.selenide.SelenideConfig;
import com.codeborne.selenide.SelenideDriver;

public class DriverFactory {
    public static SelenideDriver initDriver(TestBrowser browser) {
        SelenideConfig config = new SelenideConfig();
        config.browser(browser.getCode());
        return new SelenideDriver(config);
    }


}

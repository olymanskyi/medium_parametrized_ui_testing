package ua.tapir.uitests.parametrization.selenium;

import com.codeborne.selenide.WebDriverRunner;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestTemplateInvocationContext;
import org.junit.jupiter.engine.descriptor.TestTemplateInvocationTestDescriptor;
import org.springframework.test.util.ReflectionTestUtils;
import ua.tapir.uitests.parametrization.siteconfig.TestedPageContext;

import java.util.Arrays;
import java.util.NoSuchElementException;

public class WebdriverInitCallback implements BeforeEachCallback, AfterEachCallback {

    private JsErrorsChecker jsErrorsChecker = new JsErrorsChecker();


    @Override
    public void beforeEach(ExtensionContext ctx) {
        //create webdriver for given browser before test invocation (selenide OOTB suppports only one browser per application)
        TestedPageContext pageContext = getPageContext(ctx);
        DriverFactory.initDriver(pageContext.getBrowser());
    }

    @Override
    public void afterEach(ExtensionContext ctx) {
        jsErrorsChecker.checkJsErrors(getPageContext(ctx));
        WebDriverRunner.closeWebDriver();
    }

    //a hack. maybe, in future junit releases it will not be needed
    private TestedPageContext getPageContext(ExtensionContext extensionContext) {
        TestTemplateInvocationTestDescriptor testDescriptor = (TestTemplateInvocationTestDescriptor) ReflectionTestUtils.getField(extensionContext, "testDescriptor");
        TestTemplateInvocationContext invocationContext = (TestTemplateInvocationContext) ReflectionTestUtils.getField(testDescriptor, "invocationContext");
        Object[] arguments = (Object[]) ReflectionTestUtils.getField(invocationContext, "arguments");
        return (TestedPageContext) Arrays.stream(arguments).filter(o -> o instanceof TestedPageContext).findFirst()
                .orElseThrow(() -> new NoSuchElementException("Test method should contain TestedPageContext parameter for test data injection"));
    }
}

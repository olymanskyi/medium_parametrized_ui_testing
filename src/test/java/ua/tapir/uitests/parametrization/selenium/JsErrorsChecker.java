package ua.tapir.uitests.parametrization.selenium;

import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.tapir.uitests.parametrization.siteconfig.TestedPageContext;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;

public class JsErrorsChecker {

    private static final Logger LOG = LoggerFactory.getLogger(JsErrorsChecker.class);


    void checkJsErrors(TestedPageContext pageContext) {
        //works only in chrome. It's a good idea to check regression in javascript code
        if (pageContext.getBrowser() == TestBrowser.CHROME) {

            Set<String> errorStrings = new HashSet<>();
            errorStrings.add("SyntaxError");
            errorStrings.add("EvalError");
            errorStrings.add("ReferenceError");
            errorStrings.add("RangeError");
            errorStrings.add("TypeError");
            errorStrings.add("URIError");

            LogEntries logEntries = WebDriverRunner.getWebDriver().manage().logs().get(LogType.BROWSER);
            boolean jsErrorsFound = false;
            for (LogEntry logEntry : logEntries) {
                if (logEntry.getLevel() == Level.SEVERE) {
                    LOG.error("Js error found {} {} {} ", new Date(logEntry.getTimestamp()), logEntry.getLevel(), logEntry.getMessage());

                    if (pageContext.isFailOnJsErrors() && errorStrings.stream().anyMatch(o -> logEntry.getMessage().contains(o))) {
//                        System.out.println("Js error found: " + logEntry.getMessage());
                        jsErrorsFound = true;
                    }
                }
            }
            if (jsErrorsFound && pageContext.isFailOnJsErrors()) {
                throw new RuntimeException("Found JS errors. Fail the test. See logs for more details.");
            }
        }
    }
}

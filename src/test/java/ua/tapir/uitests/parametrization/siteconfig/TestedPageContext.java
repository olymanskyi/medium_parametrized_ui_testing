package ua.tapir.uitests.parametrization.siteconfig;

import com.codeborne.selenide.Selenide;
import ua.tapir.uitests.parametrization.selenium.TestBrowser;

/**
 * Data for current test:
 * site url + path
 * test data
 * authorization data
 * metadata for current test
 */
public class TestedPageContext {
    private String serverUrl;
    private String path;
    private TestBrowser browser;
    private String testMethodName;
    private boolean failOnJsErrors;
    //TODO other data you need in tests

    /**
     * Invoke this method in the 1 st line of a test method
     */
    public void openRootPage() {
        Selenide.open(serverUrl + path);
    }

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public TestBrowser getBrowser() {
        return browser;
    }

    public void setBrowser(TestBrowser browser) {
        this.browser = browser;
    }

    public String getTestMethodName() {
        return testMethodName;
    }

    public void setTestMethodName(String testMethodName) {
        this.testMethodName = testMethodName;
    }

    public boolean isFailOnJsErrors() {
        return failOnJsErrors;
    }

    public void setFailOnJsErrors(boolean failOnJsErrors) {
        this.failOnJsErrors = failOnJsErrors;
    }

    @Override
    public String toString() {
        return testMethodName + " " + browser + " " + path;
    }
}

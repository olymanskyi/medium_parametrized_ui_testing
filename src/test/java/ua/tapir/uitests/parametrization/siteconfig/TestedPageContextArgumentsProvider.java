package ua.tapir.uitests.parametrization.siteconfig;


import org.apache.commons.lang3.ArrayUtils;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.support.AnnotationConsumer;
import ua.tapir.uitests.parametrization.selenium.ParametrizedUiTest;
import ua.tapir.uitests.parametrization.selenium.TestBrowser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class TestedPageContextArgumentsProvider implements ArgumentsProvider, AnnotationConsumer<ParametrizedUiTest> {

    private TestBrowser[] browsers;
    private String[] paths;
    private boolean failOnJsErrors;

    private final String TESTED_SITE_BASE_URL = "https://sokyra.party";


    public void accept(ParametrizedUiTest source) {
        browsers = source.browsers();
        if (ArrayUtils.isEmpty(source.browsers())) {
            //Test all browsers in not defined
            browsers = TestBrowser.values();
        }

        paths = source.paths();
        if (ArrayUtils.isEmpty(source.paths())) {
            //Test all locales if no paths defined
            paths = SiteLocale.ALL;
        }

        failOnJsErrors = !"FALSE".equals(source.failOnJsErrors());
    }

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) {
        final String testMethod = extensionContext.getDisplayName();
        List<Arguments> arguments = new ArrayList<>();
        //run test for each defined browser * each defined path
        Arrays.stream(browsers)
                .forEach(browser -> Arrays.stream(paths)
                        .forEach(path ->
                                arguments.add(Arguments.of(createContext(browser, path, testMethod)))));

        return arguments.stream();
    }

    private TestedPageContext createContext(TestBrowser browser, String path, String testMethod) {
        //TODO you can store test data in property files or database and populate it individually for each locale or test method
        TestedPageContext ctx = new TestedPageContext();
        ctx.setBrowser(browser);
        ctx.setServerUrl(TESTED_SITE_BASE_URL);
        ctx.setPath(path);
        ctx.setTestMethodName(testMethod);
        ctx.setFailOnJsErrors(failOnJsErrors);
        return ctx;
    }
}

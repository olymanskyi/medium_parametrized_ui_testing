package ua.tapir.uitests.parametrization.siteconfig;

public interface SiteLocale {
    String UA_SITE = "/ua";
    String EN_SITE = "/en";

    String[] ALL = {UA_SITE, EN_SITE};
}

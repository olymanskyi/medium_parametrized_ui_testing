package ua.tapir.uitests.sokyrasampletests;


import com.codeborne.selenide.WebDriverRunner;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import ua.tapir.uitests.parametrization.selenium.ParametrizedUiTest;
import ua.tapir.uitests.parametrization.selenium.TestBrowser;
import ua.tapir.uitests.parametrization.siteconfig.TestedPageContext;

public class PlainSelenium {


    @Disabled
    @ParametrizedUiTest(browsers = TestBrowser.FIREFOX, paths = "/not used in this test")
    void accessPlainSeleniumWebDriver_firefox(TestedPageContext ctx) {
        //Plain selenium although works
        WebDriver driver = WebDriverRunner.getWebDriver();
        driver.get("https://examples.javacodegeeks.com/enterprise-java/selenium/selenium-tutorial-for-beginners/");
        WebElement anElement = driver.findElement(By.id("syntaxhighlighteranchor"));
        Assertions.assertNotNull(anElement, "element should not be null");
    }


}

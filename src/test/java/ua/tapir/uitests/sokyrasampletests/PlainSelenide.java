package ua.tapir.uitests.sokyrasampletests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class PlainSelenide {
//    @Disabled
    @Test
    void plainSelenideTestWithoutCustomizations_chrome() {
        open("https://github.com/selenide-examples");
        Assertions.assertTrue($("img").exists(), "page should have an image");
    }
}

package ua.tapir.uitests.sokyrasampletests;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import ua.tapir.uitests.parametrization.selenium.ParametrizedUiTest;
import ua.tapir.uitests.parametrization.selenium.TestBrowser;
import ua.tapir.uitests.parametrization.siteconfig.TestedPageContext;

import static com.codeborne.selenide.Selenide.$;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

public class ParametrizedTests {
    @ParametrizedUiTest(paths = "/en/pro-nas")
    void openAllStoresHomePageAndSearchProducts(TestedPageContext ctx) {
        ctx.openRootPage();
        boolean hasHeader = $(By.className("header_wrapper")).exists();
        boolean hasFooter = $(".footer_wrapper").exists();

//        SelenideElement searchInput = $(".form-search input[type=search]");
//        searchInput.setValue("Get the party started");
//        searchInput.pressEnter();
        $(".form-search").submit();
//        #page2
        SelenideElement searchResultElement = $(".region h2");
        searchResultElement.waitUntil(Condition.exist, 15_000);
        boolean searchPageWorks = searchResultElement.exists();

        Assertions.assertAll(
                () -> assumeTrue(hasHeader, "has header"),
                () -> assumeTrue(hasFooter, "has footer"),
                () -> assumeTrue(searchPageWorks, "has search page")
        );
    }

    @ParametrizedUiTest(failOnJsErrors = "TRUE", browsers = TestBrowser.CHROME)
    void testHomePageOnJsErros(TestedPageContext ctx) {
        ctx.openRootPage();
    }

}
